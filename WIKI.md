

The reason  why Capstone customers choose a private type of the repository is because there are risks to be stolen, so commercial code is better to keep without having it in the open. This is the rule for documents, contracts and programs that are proprietary as far as the customer doesn't want to share. Also, open-source code is vulnerable as soon as hackers can see it's weak sides.

It can be said that the Capstone Project uses Proprietary protocol. This gives the owner the option to change the protocol(code) design and implementation and enforce restrictions on the usage. Owners of the project can enforce restrictions through patents rights and trade secrets and do not disclose the technical information behind the protocol.

For businesses having financial constraints and looking to cut costs, limited tasks, open-source protocols is the way. 
Compared to customer projects, Signal's Private Messengers are performed via encryption protocol, which is used by platforms such as WhatsApp. It is an open-source protocol, which allows any security researcher to scrutinize its code for flaws and verify that the encryption is as secure as Signal claims. It is good for security reasons. 
However, this task is repetitive and not client - focused, so in case we perform a project with special tasks and needs - for the client it is better to have local, proprietary protocol. 
 
